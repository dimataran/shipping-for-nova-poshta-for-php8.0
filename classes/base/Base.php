<?php

namespace plugins\NovaPoshta\classes\base;

/**
 * Class Base
 * @package plugins\NovaPoshta\classes\base
 */
class Base
{

    /**
     * @return string
     */
    public static function getClass()
    {
        return get_called_class();
    }

    /**
     * @param $property
     * @return mixed
     */
    public function __get(string $property)
    {
//        if (array_key_exists($property, $this->shippingMethodSettings)) {
//            return $this->shippingMethodSettings[$property];
//        } else {
        $method = 'get' . ucwords($property);
//        $this->$property = $this->$method();
        return $this->$method();
//        return $this->$property;
//    }

    }
}